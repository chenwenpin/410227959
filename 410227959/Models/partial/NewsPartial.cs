﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace _410227959.Models
{
    [MetadataType(typeof(NewsMetadata))]
    public partial class News
    {
    }

    public class NewsMetadata
    {
        public int Id { get; set; }

     [DisplayName("標題")]
        [Required]
        [StringLength(50)]
        public string Title { get; set; }
        
[DisplayName("內容")]
        [Required]
        [StringLength(1000)]
        public string Content { get; set; }
        


 [DisplayName("建立時間")]
        [Required]
        public System.DateTime CreateTime { get; set; }
       
       
    }
}